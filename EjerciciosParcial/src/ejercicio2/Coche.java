package ejercicio2;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Scanner;



public class Coche {
	
	private String marca;
	private String modelo;
	private String patente;
	private int kilometros;
	
	
	Coche(){}


	public Coche(String marca, String modelo, String patente, int kilometros) {
		
		this.marca = marca;
		this.modelo = modelo;
		this.patente = patente;
		this.kilometros = kilometros;
	}


	public String getMarca() {return marca;}

	
	public void setMarca(String marca) {this.marca = marca;}

	
	public String getModelo() {return modelo;}


	public void setModelo(String modelo) {this.modelo = modelo;}


	public String getPatente() {return patente;}


	public void setPatente(String patente) {this.patente = patente;}


	public int getKilometros() {return kilometros;}


	public void setKilometros(int kilometros) {this.kilometros = kilometros;}
	
	

	
    
	public static void main(String[] args) {
		
		ArrayList<Coche> coches = new ArrayList<>();
		Scanner teclado = new Scanner(System.in);
		String marcaAuto = null;
		String modeloAuto = null;
		String patenteAuto = null;
		int kmAuto = 0;
	    boolean continuarCarga = true;
	    Coche auto;
	    
	    System.out.println("REGISTRO DE AUTOS\n");
	  //PRUEBA INGRESO POR TECLADO
		while(continuarCarga) {
			
		
			System.out.println("Ingrese Marca del auto: ");
			marcaAuto = teclado.next();
			System.out.println("Ingrese Modelo: ");
			modeloAuto = teclado.next();
			System.out.println("Ingrese Patente: ");
			patenteAuto = teclado.next();
			System.out.println("Ingrese los Kiolometros del auto: ");
			kmAuto = teclado.nextInt();
			auto = new Coche(marcaAuto, modeloAuto, patenteAuto, kmAuto);
			coches.add(auto);
			System.out.println("Desea seguir cargando datos true/false");
			continuarCarga = teclado.nextBoolean();
			
		}
		
        //teclado.close();
		
		//MOSTRA LISTA
		for (Coche coche : coches) {
			
			System.out.println("\nMarca: " + coche.marca + "\nModelo: " + coche.modelo + "\nPatente: " + coche.patente + "\nKM: " + coche.kilometros);
			
		}
		
		System.out.println("*************************************************************");
		
		System.out.println("Ingrese Marca a buscar: ");
		
		
		String buscarMarca =  teclado.next();
		
		System.out.println("\nCOCHES DE MARCA: " + buscarMarca);
		
		for (Coche coche : coches) {
			
			if(coche.marca.equals(buscarMarca)) {
				
				System.out.println("\nMarca: " + coche.marca + "\nModelo: " + coche.modelo + "\nPatente: " + coche.patente + "\nKM: " + coche.kilometros);
			}
			
		}
		
		
		
		System.out.println("*************************************************************");
		System.out.println("ORDEN POR KILOMETROS");
		Collections.sort(coches, new Comparator<Coche>() {

			@Override
			public int compare(Coche o1, Coche o2) {
				// TODO Auto-generated method stub
				return o1.kilometros - o2.kilometros;
			}
			
			
		});
		

		
		//MOSTRA LISTA
				for (Coche coche : coches) {
					
					System.out.println("\nMarca: " + coche.marca + "\nModelo: " + coche.modelo + "\nPatente: " + coche.patente + "\nKM: " + coche.kilometros);
					
				}
				
		
		System.out.println("*************************************************************");
		
		//ORDEN POR MARCA
		System.out.println("ORDEN MARCA");
		Collections.sort(coches, new Comparator<Coche>() {

			@Override
			public int compare(Coche o1, Coche o2) {
				// TODO Auto-generated method stub
				return o1.marca.compareTo(o2.marca);
			}
			
		});
		
		
		//MOSTRA LISTA
		for (Coche coche : coches) {
			
			System.out.println("\nMarca: " + coche.marca + "\nModelo: " + coche.modelo + "\nPatente: " + coche.patente + "\nKM: " + coche.kilometros);
			
		}
		
		
		
	}

}
