package ejericicio1;

import java.util.Random;

public class Cuenta {
	
	
	private String titular;//es obligatorio
	private float cantidad = 0f;
	
	
	Cuenta(){}
	
	
	Cuenta(String titular){
		
		this.titular = titular;
	}


	public String getTitular() {return titular;}

	
	public void setTitular(String titular) {this.titular = titular;}
	
	
	public void ingresar(float cantidad) {
		
		if(cantidad > 0) {
		
		this.cantidad += cantidad;
		
		}
		
	}
	
	
	public void mostrar() {
		
		System.out.println("Titular: " + titular + "\nCantidad: " + cantidad);
	}
	
	
	public String retirar(float cantidad) {
		
		
		if(this.cantidad <=  0f) {
			
			return "La cuenta puede estar en Numeros rojos";
			
		}
		
		if(this.cantidad < cantidad) {
			
			return "No cuenta con saldo suficente";
		}
		
		this.cantidad = this.cantidad - cantidad;
		
		return "Le queda saldo: "+ this.cantidad;
	}
	
	
	
	
	
	//************************************************************************************************************
	
    public static void main(String[] args) {
    	
    	Random rdm = new Random();
		
    	Cuenta cuentaPedro = new Cuenta("Pedro Juarez");
    	
    	String retirar = null;
    	
    	//VERIFICO SI LA CUENTA NO TIENE SALDO PARA RETIRAR
    	retirar = cuentaPedro.retirar(rdm.nextFloat()*1000);
    	
    	System.out.println(retirar);
    	
    	cuentaPedro.ingresar(rdm.nextFloat()*2000);
    	
    	cuentaPedro.mostrar();
    	
    	retirar = cuentaPedro.retirar(10f);
    	
    	System.out.println(retirar);
    	
    	System.out.println("**********************************************************");
    	
        Cuenta cuentaNadia = new Cuenta("Nadia");
    	
    	String retirarCuentaNadia = null;
    	
    	cuentaNadia.ingresar(1000f);
    	
    	cuentaNadia.mostrar();
    	
    	retirarCuentaNadia = cuentaNadia.retirar(10f);
    	
    	System.out.println(retirarCuentaNadia);;
    	
    	
	}
	
	

}
