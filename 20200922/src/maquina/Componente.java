package maquina;


public class Componente {
	
	private String nroSerie;
	private String descripcion;
	private float peso;
	
	
	Componente(String nroSerie, String descripcion, float peso){
		
		this.nroSerie = nroSerie;
		this.descripcion = descripcion;
		this.peso = peso;
	}


	public String getNroSerie() {return nroSerie;}


	public void setNroSerie(String nroSerie) {this.nroSerie = nroSerie;}


	public String getDescripcion() {return descripcion;}


	public void setDescripcion(String descripcion) {this.descripcion = descripcion;}


	public float getPeso() {return peso;}


	public void setPeso(float peso) {this.peso = peso;}


	@Override
	public String toString() {
		
		return "\nComponente -> nroSerie: " + nroSerie + ", descripcion: " + descripcion + ", peso: " + peso;
	}
	
	
	
	

}
