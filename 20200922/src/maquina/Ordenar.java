package maquina;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

public class Ordenar {
	
	public static void ordenarComponentes(ArrayList<Componente> componentes) {
		
		Collections.sort(componentes, new Comparator<Componente>() {
			   @Override
			   public int compare(Componente o1, Componente o2) {
				   
				   return o1.getDescripcion().compareTo(o2.getDescripcion());
			   }
		});
	}
		
		
	public static void ordenarMaquinas(ArrayList<Maquina> maquinas) {
			
		Collections.sort(maquinas, new Comparator<Maquina>() {

			@Override
			public int compare(Maquina o1, Maquina o2) {
				
				return o1.getPesoTotal().compareTo(o2.getPesoTotal());
			}
				
			});
		
	}

}
