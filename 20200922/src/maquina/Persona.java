package maquina;

public class Persona {
	
	private long dni; 
	private String nombre; 
	
	Persona(long dni, String nombre){
		
		this.dni = dni;
		this.nombre = nombre;
	}
	
	
	public void setDni(long dni) {this.dni = dni;}

	
	public long getDni() {return this.dni;}

	
	public void setNombre(String nombre) {this.nombre = nombre;}
	
	
	public String getNombre() {return this.nombre;}


	@Override
	public String toString() {
		
		return "dni: " + dni + ", nombre: " + nombre;
	}




}
