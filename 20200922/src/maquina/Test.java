package maquina;

import java.util.ArrayList;

public class Test {
	
	
	public static void main(String[] args) {
		
		ArrayList<Maquina> maquinas = new ArrayList<>();
		
		Persona p = new Persona(4852335, "Jose");
		
		Componente disco = new Componente("002", "LG disco rigido 500GB", 300f);
		Componente chipset = new Componente("009", "Intel", 100f);
		Componente memoria = new Componente("008", "Kingston memoria ram", 200f);
		Componente fuente = new Componente("004", "Fuente", 250f);
		
		Maquina pc1 = new Maquina("252");
		//AGREGO COMPONENTES A LA PC
		pc1.agragarComponentes(disco);
		pc1.agragarComponentes(chipset);
		pc1.agragarComponentes(memoria);
		pc1.agragarComponentes(fuente);
		//AGREGO ENCARGADO DE LA PC
		pc1.setEncargado(p);
	
		//pc1.ordenarComponentes();
		Ordenar.ordenarComponentes(pc1.getComponentes());
       
		maquinas.add(pc1);
	  
	   Maquina bicicleta = new Maquina("288");
	  
	   bicicleta.agragarComponentes(new Componente("005", "Ruedas", 8f));
	   bicicleta.agragarComponentes(new Componente("008", "Manivelas", 2f));
	   bicicleta.agragarComponentes(new Componente("010", "Engranajes", 2f));	  
	   bicicleta.setEncargado(new Persona(25685566, "Santos"));
	  
	  //bicicleta.ordenarComponentes();
	   Ordenar.ordenarComponentes(bicicleta.getComponentes());
	 
	   maquinas.add(bicicleta);
	  
	   System.out.println("**********COMPONENTES ORDENADOS POR SU DESCRIPCION*********\n");
	   System.out.println(maquinas);
	   System.out.println("\n");
	   
	  //ORDENAR MAQUINAS POR PESO
	   
	   Ordenar.ordenarMaquinas(maquinas);
	   System.out.println("**********MAQUINAS ORDENADAS POR SU PESO TOTAL*********\n");
	   System.out.println(maquinas);
	  
	}

	
}
