package maquina;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

public class Maquina{
	

	private String id;
	private Persona encargado;
	private ArrayList<Componente>componentes = null;
	
	
	public Maquina(String id) {
		
		this.id = id;
	}
	
	
	public void setId(String id) { this.id = id;}
	

	public String getId() {return this.id;}


	public Persona getEncargado() {return encargado;}


	public void setEncargado(Persona encargado) {this.encargado = encargado;}


	public ArrayList<Componente> getComponentes() {return componentes;}
	
	
	public void agragarComponentes(Componente componente) {
		
		if(componentes == null) {
			
			componentes = new ArrayList<>();
		}
		
		componentes.add(componente);
	}
	
	
	public Float getPesoTotal() {
		
		float total = 0;
		
		for (Componente componente : componentes) {
			
			total += componente.getPeso();
		}
		
		return total;
	}
	
	
	public void ordenarComponentes() {
		
		Collections.sort(componentes,new Comparator<Componente>() {
			   @Override
			   public int compare(Componente o1, Componente o2) {
				   
				   return o1.getDescripcion().compareTo(o2.getDescripcion());
			   }
		});
	}
	

	@Override
	public String toString() {
		return "\nMaquina \nid: " + id + "\nencargado: " + encargado + "\ncomponente: " + componentes ;
	}
	
}
