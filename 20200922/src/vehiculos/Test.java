package vehiculos;

import java.util.Random;

public class Test {
	
	
	public static void main(String[] args) {
		
		Random rdm = new Random();
		
		Vehiculo v1 = new Auto();
		Vehiculo v2 = new Camioneta();
		Vehiculo v3 = new Colectivo();
		
		
		
		System.out.println(v1.getCapacidad(rdm.nextInt(5) + 1));
		System.out.println(v1.consumir(100f));
		
		System.out.println(v2.getCapacidad(rdm.nextInt(1000) + 10));
	    System.out.println(v2.consumir(100f));
	    
	    int cantPasajeros = rdm.nextInt(25) + 1;
	    
	    for(int i = 0; i < cantPasajeros; i++) {
	    	
	    	((Colectivo)v3).abordar(new Pasajero(rdm.nextFloat() * 100f + 20f));
	    }
	    
	    ((Colectivo)v3).mostrarPasajeros();
	    
	    System.out.println(v3.getCapacidad(cantPasajeros));
	    System.out.println(v3.consumir(100f));
	    
	   
	  
	}	

}
