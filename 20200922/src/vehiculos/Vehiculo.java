package vehiculos;

public class Vehiculo {
		
	private String id;
	private String marca;
	private String modelo;

	
	
	public String getId() {return id;}
	
	
	public void setId(String id) {this.id = id;}
	
	
	public String getMarca() {return marca;}
	
	public void setMarca(String marca) {this.marca = marca;}
	
	
	public String getModelo() {return modelo;}
	
	
	public void setModelo(String modelo) {this.modelo = modelo;}
	
	
	public String getCapacidad(int capacidad) {
		// TODO Auto-generated method stub
		return "Mi capacidad es: " + capacidad;
	}
	
	
	public float calcularConsumo(float dato) {
		
		return  5f * dato;
	}
	
	
   public String consumir(float cantidadKM) {
		
		return "Mi consumo es: " + calcularConsumo(cantidadKM);
	}
   
   
   public String consumir(float cantidadKM, float consumo) {
		
		return "Mi consumo es: " + (calcularConsumo(cantidadKM) + consumo);
	}

	
}
