package vehiculos;

public class Pasajero {
	
	
	private float peso;
	
	
	public Pasajero(float peso) {
		
		this.peso = peso;
	}

	public float getPeso() {return peso;}

	
	public void setPeso(float peso) {this.peso = peso;}

	@Override
	public String toString() {
		return "Pasajero [peso=" + peso + " Kg" + "]";
	}
	
	

}
