package vehiculos;

import java.util.ArrayList;
import java.util.List;

public class Colectivo extends Vehiculo{
	
	
	private List<Pasajero> pasajeros;
	
	
	public void abordar(Pasajero p) {
		
		if(pasajeros == null) {
			
			pasajeros = new ArrayList<>();
			
		}
		
		pasajeros.add(p);
		
	}
	
	
	public void mostrarPasajeros() {
		
		for (Pasajero pasajero : pasajeros) {
			 
			System.out.println(pasajero);
		}
	}
	
	
	 public String getCapacidad(int capacidad) {
			
		return "Hola soy Colectivo y "+ super.getCapacidad(capacidad) + " Personas";
	}
	    
	 
		
	public String consumir(float cantidadKM) {
		
        float consumo = 0;
		 
		for (Pasajero pasajero : pasajeros) {
			
			 consumo += pasajero.getPeso() * 0.01f; 
		}
			
		return super.consumir(cantidadKM, consumo);
	}
	

}
