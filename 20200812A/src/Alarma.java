public class Alarma {
	
	
	private Sensor sensor;
	private Timbre timbre = new Timbre();
	
	
	//DEPENDECIA ABSOLUTA
	public Alarma() {
		
		sensor = new Sensor();
	}
	
	
	public void alarmar(int se�al) {
		
		if(sensor.detectarSe�al(se�al)) {
			
			System.out.println(timbre.timbrar());
			
		}else {
			
			System.out.println("Todo normal");
		}
		
		
		
	}

	
}
