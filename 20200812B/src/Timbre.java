import java.io.File;
import java.io.IOException;

import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.Clip;
import javax.sound.sampled.LineUnavailableException;
import javax.sound.sampled.UnsupportedAudioFileException;

public class Timbre {
	
	
	public String timbrar() {	
		
//***************** SONIDO DE ALARMA ****************************************************************************
		String timbre = null;
	    //Ruta del archivo
	    String sonidoAlarma = "C:......./beacon.wav";
	    Clip sonido = null;
		File fichero = new File(sonidoAlarma);
		
		try {

			//Permite precargar el Audio
			sonido= AudioSystem.getClip();
			sonido.open(AudioSystem.getAudioInputStream(fichero));
			sonido.start();
			Thread.sleep(9500);
			timbre = "Se activo la alarma exitosamente";
			
		} catch (LineUnavailableException | IOException | UnsupportedAudioFileException | InterruptedException e) {
			
		    timbre =  "Error:" + e;
			
		} finally {
			
			sonido.close();
		}
		
		return timbre;		
//****************************************************************************************************************		
		}
	
	
}
