public class Alarma {
	
	
	private Sensor sensor;
	private Timbre timbre = new Timbre();
	
	
	public Alarma() {
		
		sensor = new Sensor();
	}
	
	
	public boolean detectarSenial(int senial) {
		
		return sensor.detectarSenial(senial);
		
	}
	
	
	public void activarTimbre() {
		
		System.out.println(timbre.timbrar());
		
	}
	
	
	public void alarmar(int senial) {
		
		if(detectarSenial(senial)) {
			
			activarTimbre();
			
		}else {
			
			System.out.println("TImbre: Todo normal");
		}		
		
	}

	
}
