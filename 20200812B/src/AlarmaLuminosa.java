public class AlarmaLuminosa extends Alarma {
	
	
	private Bombilla bombilla = new Bombilla();
	
	
	public void activarBombilla() {
		
		System.out.println(bombilla.iluminar());
	}
	
	
	public void alarmar(int senial) {
		
		if(this.detectarSenial(senial)) {
		
		  this.activarBombilla();
		  this.activarTimbre();
			
		}else {
			
		   System.out.println("Timbre: Todo normal");
		   System.out.println("Bombilla: Todo normal");
		   
		}
		
	}
	
	
}
