import java.util.Random;

public class PruebaB {
	
	
	public static void main(String[] args) {
		
		Random umbralAleatorio = new Random();
		int umbral = 1 + umbralAleatorio.nextInt(6);
		AlarmaLuminosa alarmaLuminosa = new AlarmaLuminosa();
		alarmaLuminosa.alarmar(umbral);
		
	}

	
}
