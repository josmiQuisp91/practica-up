package RepasoAgregacion;

import java.util.HashSet;
import java.util.Set;

public class Estudiante {
	
	private String id;
	private String nombre;
	private Set<Curso> cursos = new HashSet<>();
	private Universidad universidad;
	private Departamento departamento;
	
	
	public Estudiante(String id, String nombre) {
	
		this.id = id;
		this.nombre = nombre;
	}


	public String getId() {return id;}
	
	
	public void setId(String id) {this.id = id;}
	
	
	public String getNombre() {return nombre;}
	
	
	public void setNombre(String nombre) {this.nombre = nombre;}
	
	
	public void setCursos(Curso curso) { this.cursos.add(curso);}
	
	
	public void setUniversidad(Universidad universidad) { this.universidad = universidad; }

	
	public void setDepartamento(Departamento departamento) { this.departamento = departamento;}
	
	
	public Departamento getDepartamento() { return this.departamento; }

	
	public Universidad getUniversidad() { return this.universidad; }
	
	
	public Set<Curso> getCursos() {return cursos;}

	@Override
	public String toString() {
		return "\nEstudiante: " + id + " nombre: " + nombre;
	}
	
	
	
	

}
