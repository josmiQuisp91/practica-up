package RepasoAgregacion;

import java.util.HashSet;
import java.util.Set;

public class Universidad {
	
	private String nombre;
	private String direccion;
	//CONJUNTO DE DEPARTAMENTOS A DEFINIR DE LA UNIVERSIDAD
	private Set<Departamento> departamentos = new HashSet<>();
	private Set<Estudiante> estudiantes = new HashSet<>();
	
	
	public Universidad(String nombre, String direccion) {
		
		this.nombre = nombre;
		this.direccion = direccion;
	}
	
	
	public String getNombre() {return nombre;}
	
	
	public void setNombre(String nombre) {this.nombre = nombre;}
	
	
	public String getDireccion() {return direccion;}
	
	
	public void setDireccion(String direccion) {this.direccion = direccion;}
	
	
	public void setDepartamentos(Departamento departamento) {this.departamentos.add(departamento);}
	
	
	public void setEstudiantes(Estudiante estudiante) {this.estudiantes.add(estudiante);}
	
	
	public Departamento buscarDepartamento(String codigo) {
		
		for (Departamento departamento : departamentos) {
			
			if(departamento.getCodigo().equals(codigo)) {
				
				return departamento;
			}
			
		}
		
		return null;
	}
	

	
	public void matricularEstudiante(Estudiante estudiante, Departamento departamento) {
		
		//AGREGO Y RELACIONO 
		this.estudiantes.add(estudiante);
		estudiante.setUniversidad(this);
		departamento.setEstudiantes(estudiante);
		estudiante.setDepartamento(departamento);
		
	}
	
}
