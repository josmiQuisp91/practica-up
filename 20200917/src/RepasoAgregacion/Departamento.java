package RepasoAgregacion;

import java.util.HashSet;
import java.util.Set;

public class Departamento {
	
	
	private String codigo;
	private String nombre;
	Universidad universidad;
	private Set<Profesor> profesores = new HashSet<>();
	private Set<Estudiante> estudiantes = new HashSet<>();
	private Set<Curso> cursos = new HashSet<>();
	
	
	public Departamento(String codigo, String nombre) {
		
		this.codigo = codigo;
		this.nombre = nombre;
	}


	public String getCodigo() {return codigo;}
	
	
	public void setCodigo(String codigo) {this.codigo = codigo;}
	
	
	public String getNombre() {return nombre;}
	
	
	public void setNombre(String nombre) {this.nombre = nombre;}
	
	
	public void setUniversidad(Universidad universidad) {this.universidad = universidad;}
	
	
	public void setEstudiantes(Estudiante estudiante) {this.estudiantes.add(estudiante);}

   
	public Universidad getUniversidad() { return this.universidad; }

	
	public Set<Profesor> getProfesores() { return this.profesores; }
	
	
	public Set<Estudiante> getEstudiantes() { return this.estudiantes; }
	   
	
    public Profesor buscarProfesor(String id) { 
    	
    	for (Profesor profesor : profesores) {
    		
    		if(profesor.getId().equals(id)) {
    			
    			return profesor;
    		
    		}                                               
        }                                                   
    
        return null;
        
    }  
    
    
    public String mostrarProfesores() {
    	
    	String profesoresMostrar = "";
    	
    	for (Profesor profesor : profesores) {
			
    		profesoresMostrar += profesor.getId() +" "+ profesor.getNombre() +" " + profesor.getApellido()+ "\n";
		}
    	
    	return profesoresMostrar;
    }
	
    
    
    public String mostrarCursos() {
    	
    	String cursosMostrar = "";
    	
    	for (Curso curso : cursos) {
			
    		cursosMostrar += curso.getCodigo() +" " +curso.getNombre() + "\n";
		}
    	
    	return cursosMostrar;
    }
    
    
    
    public String mostrarEstudiante() {
    	
    	String estudiantesMostrar = "";
    	
    	for (Estudiante estudiante : estudiantes) {
			
    		estudiantesMostrar += estudiante.getId() +" " +estudiante.getNombre() + "\n";
		}
    	
    	return estudiantesMostrar;
    }
	
    
    
    public Curso buscarCurso(String codigo) { 
    	
    	for (Curso curso : cursos) {
    		
    		if(curso.getCodigo().equals(codigo)) {
    			
    			return curso;
    		}                                               
        }                                                   
    
        return null;
        
    }        
    
    
    public Estudiante buscarEstudiante(String codigo) { 
    	
    	for (Estudiante estudiante : estudiantes) {
    		
    		if(estudiante.getId().equals(codigo)) {
    			
    			return estudiante;
    		}                                               
        }                                                   
    
        return null;
        
    }                                                       
    
    
	
	public void registrarCursos(Curso curso, Profesor profesor) {
		
		this.cursos.add(curso);
		curso.setDepartamento(this);
		curso.setProfesor(profesor);
		profesor.setCursos(curso);
	
	}


	public void asignarProfesor(Profesor profesor) {
		
		this.profesores.add(profesor);
		profesor.setDepartamento(this);
	}	
	
	
}
