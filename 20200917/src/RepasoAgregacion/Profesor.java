package RepasoAgregacion;

import java.util.HashSet;
import java.util.Set;

public class Profesor {
	
	
	private String id;
	private String nombre;
	private String apellido;
	Departamento departamento;
	Set<Curso> cursos = new HashSet<>();
	
	
	public Profesor(String id, String nombre, String apellido) {
		
		this.id = id;
		this.nombre = nombre;
		this.apellido = apellido;
	}


	public String getId() {return id;}
	
	
	public void setId(String id) {this.id = id;}
	
	
	public String getNombre() {return nombre;}
	
	
	public void setNombre(String nombre) {this.nombre = nombre;}
	
	
	public String getApellido() {return apellido;}
	
	
	public void setApellido(String apellido) {this.apellido = apellido;}

	
	public void setDepartamento(Departamento departamento) {this.departamento = departamento;}
	
	
	public void setCursos(Curso curso) {this.cursos.add(curso);}


	
}
