package RepasoAgregacion;

import javax.swing.JOptionPane;

public class Test {
	
	public static void main(String[] args) {
		
		//MATRICULAR ALUMNOS A LA UNIVERSIDAD
		 Universidad universidad = new Universidad("Universida de Palermo", "Mario Bravo 1050");
		//DEFINO LAS UNIVERSIDADES POR DEFECTO DE LA UNIVERSIDAD
		 universidad.setDepartamentos(new Departamento("001", "Departamento de Ingenieria"));
		 universidad.setDepartamentos(new Departamento("002", "Departamento de Medicina"));
		 universidad.setDepartamentos(new Departamento("003", "Departamento de Dise�o"));
		 universidad.setDepartamentos(new Departamento("004", "Departamento de Economia"));
		
		 int opcion = 0;
		 boolean salir = false;
		 String menu = null;
		 String departamentos = "\n001 Ingenieria\n002 Medicina\n003Dise�o\n004 Economia\n\n";
		 String nombre = null;
		 String codigo = null, id = null;
		 String apellido = null;
		 
		 while(!salir) {
			 
			 menu = universidad.getNombre()+"\n"+
			 "Direccion: "+ universidad.getDireccion()+"\n\n"+
		     "1. Matricular Alumnos a la Universidad.\r\n" + 
			 "2. Asignar profesores a un departamento.\r\n" + 
			 "3. Crear cursos en el sistema.\r\n" + 
			 "4. Asignar alumnos a un curso. (inscribir)\r\n" + 
			 "5. Listar los datos de un determinado curso (Mostrar datos del profesor y todos los alumnos\r\n" +  
			 "6. Listar todos los cursos en los que esta matriculado un determinado estudiante.\n"+
			 "7. Salir\n\n"+
			 "Ingrese opcion: ";
			 
			 opcion = Integer.valueOf(JOptionPane.showInputDialog(menu));
			 
			 switch (opcion) {
			 
			case 1:		
			
				codigo = JOptionPane.showInputDialog("CODIGO NUEVO ALUMNO");
				nombre = JOptionPane.showInputDialog("NOMBRE NUEVO ALUMNO");
				
				
				Estudiante estudiante = new Estudiante(codigo, nombre);
				
				codigo = JOptionPane.showInputDialog("DEPARTAMENTO EN EL QUE VA A MATRICULAR AL NUEVO ALUMNO "+ departamentos + "\nCOD. DEPARTAMENTO");
			    
			    Departamento departamento1 = universidad.buscarDepartamento(codigo);
								
				if(departamento1 == null) {
				
					JOptionPane.showMessageDialog(null, "ERROR CODIGO INEXISTENTE");
					break;
				}
				
				universidad.matricularEstudiante(estudiante, departamento1);
				
				JOptionPane.showMessageDialog(null, "EL NUEVO ALUMNO SE MATRICULO CON EXITO");
				
				break;
			
			case 2:
				
			    codigo = JOptionPane.showInputDialog("DEPARTAMENTO EN EL QUE VA A ASIGNAR AL PROFESOR"+departamentos + "\nCOD. DEPARTAMENTO ");
			    
			    Departamento departamento2 = universidad.buscarDepartamento(codigo);
			    
				if(departamento2 == null) {
					
					JOptionPane.showMessageDialog(null, "ERROR CODIGO INEXISTENTE");
					break;
				}
			    
				
			    id = JOptionPane.showInputDialog("ID PROFESOR NUEVO.: ");
			    nombre = JOptionPane.showInputDialog("NOMBRE PROFESOR NUEVO");
			    apellido = JOptionPane.showInputDialog("APELLIDO PROFESOR NUEVO");
			    
			    departamento2.asignarProfesor(new Profesor(id, nombre, apellido));
			    
			    JOptionPane.showMessageDialog(null, "SE ASIGNO CON EXITO AL PROFESOR CON ID: "+ id+ " EN EL DEPARTAMENTO DE "+ departamento2.getNombre());
			    
				break;
				
			case 3:
				
			    codigo = JOptionPane.showInputDialog("INGERSE COD. DEPARTAMENTO A ASIGNAR EL CURSO"+departamentos + "\nCODIGO");
	
			    Departamento departamento3 = universidad.buscarDepartamento(codigo);
								
				if(departamento3 == null) {

					JOptionPane.showMessageDialog(null, "ERROR DEPARTAMENTO INEXISTENTE");
					break;
				}
				
				codigo = JOptionPane.showInputDialog("CODIGO NUEVO CURSO");
				nombre = JOptionPane.showInputDialog("NOMBRE NUEVO CURSO");
				    
				Curso curso3 = departamento3.buscarCurso(codigo);
				    
                if(curso3 != null) {
						
					JOptionPane.showMessageDialog(null, "EL CURSO:" +codigo+ " YA EXISTE");
					break;
				}
                
                String mostrarProfesores = departamento3.mostrarProfesores();
                
                if (mostrarProfesores == "") {
					
          					JOptionPane.showMessageDialog(null, "ERROR NO SE ENCONTRO NINGUN PROFESOR REGISTRADO EN ESTE DEPARTAMENTO");
          					break;
          	     }
                              
                
                id = JOptionPane.showInputDialog("INGRESE ID PROFESOR QUE VA ASIGNAR AL CURSO\n"+mostrarProfesores+"\nID");
               
                Profesor p = departamento3.buscarProfesor(id);
                
                if(p == null) {
                	
                	JOptionPane.showMessageDialog(null, "ERROR PROFESOR INEXISTENTE");
					break;
                }
                    
				departamento3.registrarCursos(new Curso(codigo, nombre), p);
				    
				JOptionPane.showMessageDialog(null, "NUEVO CURSO REGISTRO EXITOSO");
				
				break;
				
			case 4:
				
			    codigo = JOptionPane.showInputDialog("CODIGO DEL DEPARTAMAENTO DONDE SE ENCUENTRA EL CURSO" +departamentos + "CODIGO ");

			    Departamento departamento4 = universidad.buscarDepartamento(codigo);
			    
				if(departamento4 == null) {
					
					JOptionPane.showMessageDialog(null, "ERROR DEPARTAMENTO " + codigo + " INEXISTENTE");
					break;
				}
				
				
				String mostrarCursos = departamento4.mostrarCursos();
				
				if (mostrarCursos == "") {
					
					JOptionPane.showMessageDialog(null, "ERROR NO SE ENCONTRARON CURSOS REGISTRADOS");
					break;
				}
				
				codigo = JOptionPane.showInputDialog(mostrarCursos+"\nINGRESE COD. CURSO EN EL QUE VA ASIGNAR ALUMNO: ");
				    
				Curso curso4 = departamento4.buscarCurso(codigo);
				
				if(curso4 == null) {
					
					JOptionPane.showMessageDialog(null, "ERROR CURSO INEXISTENTE");
					break;
				}
			
				
				String mostrarEstudiantes = departamento4.mostrarEstudiante();
				
				id = JOptionPane.showInputDialog("INGRESE COD ALUMNO QUE VA A INSCRIBIR EN EL CURSO\n"+mostrarEstudiantes+"\nID");
				
				Estudiante estudiante4 = departamento4.buscarEstudiante(id);
				
				if(estudiante4 == null) {
					
					JOptionPane.showMessageDialog(null, "ERROR ALUMNO NO MATRICULADO EN EL SISTEMA");
					break;
				}
                 
				curso4.asignarEstudiantes(estudiante4);
				JOptionPane.showMessageDialog(null, "EL PROCESO DE INSCRIPCION "+ curso4.getNombre() + "SE REALIZO CON EXITO");
				break;
			
			case 5:
				
				codigo = JOptionPane.showInputDialog(departamentos + "Codigo de departamento: ");
				    
				Departamento departamento5 = universidad.buscarDepartamento(codigo);
				    
				if(departamento5 == null) {
						
					JOptionPane.showMessageDialog(null, "DEPARTAMENTO INEXISTENTE");
					break;
				}
					
					
			   String mostrarCursos5 = departamento5.mostrarCursos();
			   

				if (mostrarCursos5 == "") {
					
					JOptionPane.showMessageDialog(null, "ERROR NO SE ENCONTRO NINGUN CURSO REGISTRADO EN ESTE DEPARTAMENTO");
					break;
				}
				
			   codigo = JOptionPane.showInputDialog(mostrarCursos5+"\nCOD CURSO A MOSTRAR: ");
					    
			   Curso curso5 = departamento5.buscarCurso(codigo);
					
			   if(curso5 == null) {
				   
				   JOptionPane.showMessageDialog(null, "ERROR CURSO INEXISTENTE");
						break;
			   }
			   
			   JOptionPane.showMessageDialog(null,"Curso ->" +curso5.getCodigo() +" "+ curso5.getNombre() + "\n Profesor ->" + curso5.getProfesor().getNombre()+
					   " "+curso5.getProfesor().getApellido() +"\nDepartamento "+curso5.getDepartamento().getCodigo() +" "+curso5.getDepartamento().getNombre()+
					  "\n" + curso5.getEstudiantes());
			   break;
			   
			case 6:
				
				codigo = JOptionPane.showInputDialog("INGRESE COD. DEPARTAMENTO"+ departamentos + "Codigo de departamento: ");
			    
				Departamento departamento6 = universidad.buscarDepartamento(codigo);
				    
				if(departamento6 == null) {
						
					JOptionPane.showMessageDialog(null, "DEPARTAMENTO INEXISTENTE");
					break;
				}
				
				String mostrarEstudiantes6 = departamento6.mostrarEstudiante();
				
				id = JOptionPane.showInputDialog("INGRESE ID ALUMNO\n"+mostrarEstudiantes6+"\nID");
				
				Estudiante estudiante6 = departamento6.buscarEstudiante(id);
				
				if(estudiante6 == null) {
					
					JOptionPane.showMessageDialog(null, "ERROR ID NO MATRICULADO EN EL SISTEMA");
					break;
				}
				
				 JOptionPane.showMessageDialog(null,estudiante6.getNombre()+"\nCURSOS EN LOS QUE SE ENCUENTRA INSCRIPTO\n"+estudiante6.getCursos());
				 break;
				
			case 7:
				
				salir = true;
				break;					
			        
		 }
		 
	  }
		 
		 
	
		 }
}