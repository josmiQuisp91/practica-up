package RepasoAgregacion;

import java.util.HashSet;
import java.util.Set;

public class Curso {
	
	
	private String codigo;
	private String nombre;
	Profesor profesor;
	Departamento departamento;
	private Set<Estudiante> estudiantes = new HashSet<>();
	
	
	public Curso(String codigo, String nombre) {
		
		this.codigo = codigo;
		this.nombre = nombre;
	}


	public String getCodigo() {return codigo;}
	
	
	public void setCodigo(String codigo) {this.codigo = codigo;}					
	
	
	public String getNombre() {return nombre;}
	
	
	public void setNombre(String nombre) {this.nombre = nombre;}

	
	public void setProfesor(Profesor profesor) { this.profesor = profesor;}

	
	public void setDepartamento(Departamento departamento) { this.departamento = departamento;}
	
	
	public Departamento getDepartamento() { return this.departamento;}
	
	
	public Profesor getProfesor() {return this.profesor;}
	
	
	public Set<Estudiante> getEstudiantes() {return estudiantes;}


	public void asignarEstudiantes(Estudiante estudiante) {
		
		this.estudiantes.add(estudiante);
		estudiante.setCursos(this);
		
	}


	@Override
	public String toString() {
		
	    return "Curso: " + codigo + "\t" + nombre + "\n";
	    
	}



	
}

