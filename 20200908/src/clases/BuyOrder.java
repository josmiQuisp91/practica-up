package clases;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

//ORDEN DE COMPRA
public class BuyOrder implements Comparable<BuyOrder> {
	
	
	private Date date;
	private int number;
	Provider provider;
	private List<Item> products;
	
	
	public BuyOrder(int number, String lastName , String name, String address) {
		
		this.date = new Date();
		this.number = number;
		this.provider = new Provider(lastName, name, address);
		
	}
	
	
	public Date getDate() {return date;}

	
	public void setDate(Date date) {this.date = date;}

	
	public int getNumber() {return number;}

	
	public void setNumber(int number) {this.number = number;}
	
	
	public List<Item> getProducts() {return products;}
	
	
	public Provider getProvider() {return provider;}


	// Cada vez que instancie Lista retorna a null
	public void setProducts(Item p) {
		
		if(products == null) {	
			
			products = new ArrayList<>();
			
		}
		
		products.add(p);
		
	}
	
	@Override
	public int compareTo(BuyOrder o) {
		
		return this.number - o.number;
		
	}

	@Override
	public String toString() {
		
		return "\nBuyOrder [date= " + date +  "\n number order= " + number + "\n provider= " + provider + "\n\n products= " + products
			+ "\n\n******************************************************" 	+ "]";
		
	}
	
	
}
