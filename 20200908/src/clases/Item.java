package clases;

public class Item {
	
	
	private String description;
	private float price;
	private int quantity;
	
	
	public Item(String description, float price, int quantity) {
		
		this.description = description;
		this.price = price;
		this.quantity = quantity;
		
	}
	

	public String getDescription() {return description;}
	
	
	public void setDescription(String description) {this.description = description;}
	
	
	public float getPrice() {return price;}
	
	
	public void setPrice(float price) {this.price = price;}
	
	
	public int getQuantity() {return quantity;}
	
	
	public void setQuantity(int quantity) {this.quantity = quantity;}
	
	@Override
	public String toString() {
		
		return "\ndescription= " + description + "\n price= " + price + "$" + "\n quantity= " + quantity;
	
	}
	
	
}
