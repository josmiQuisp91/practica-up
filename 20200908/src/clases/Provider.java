package clases;

public class Provider{
	
	
	private String lastname;
	private String name;
	private String address;
	
	
	public Provider(String lastname, String name, String address) {
		
		this.lastname = lastname;
		this.name = name;
		this.address = address;
		
	}
	

	public String getLastname() {return lastname;}
	
	
	public void setLastname(String lastname) {this.lastname = lastname;}
	
	
	public String getName() {return name;}
	
	
	public void setName(String name) {this.name = name;}
	
	
	public String getAddress() {return address;}
	
	
	public void setAddress(String address) {this.address = address;}

	@Override
	public String toString() {
		
		return "Provider [lastname=" + lastname + ", name=" + name + ", address=" + address + "]";
		
	}
	
	
}
