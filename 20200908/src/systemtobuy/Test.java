package systemtobuy;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Random;
import java.util.Set;

import clases.BuyOrder;
import clases.Item;
import dataEnum.Providers;
import dataEnum.Items;



public class Test {
	
	
	public static void main(String[] args) {
		//SISTEMA
		Register system = Register.getInstance();
		Random rand = new Random();
	
//GENERADOR DE ORDENES DE COMPRA
		//VALORES DE PROVEEDORES Y ITEMS PREDEFINIDAS
		Providers[] provider = Providers.values();
	    Items[] products = Items.values();
	    //LISTAS DE ITEM Y PROVEEDORES ALEATORIOS
	    int quantity = rand.nextInt(8) + 5;
        Set<Integer> generatorProvider = NumberRdm.generator(quantity);
        Set<Integer> generatorProducts = NumberRdm.generator(quantity);
        
       
       // System.out.println(generatorProducts);
       // System.out.println(generatorProvider);
        //(int)Obj_Integer
        //ObjInteger.intValue()
		
	    
	    BuyOrder order; 
	    int generator;
	    int generator1;
	    
	    
	    for (Integer ni : generatorProvider) {
	    	
	    	generator = ni.intValue();
	    	order = new BuyOrder(rand.nextInt(10000)+1 * generator, provider[generator].getLastName(), provider[generator].name(), provider[generator].getAddress());
		    
	    	for (Integer nj : generatorProducts) {
				
	    		generator1 = nj.intValue();
	    		order.setProducts(new Item(products[generator1].getDescription(), products[generator1].getPrice(), rand.nextInt(10) + 1));
			}	
	    	
	    	generatorProducts = NumberRdm.generator(quantity);
	    	//REGISTRO DE ORDEN DE COMPRA EN EL SISTEMA
	    	 system.register(order);
		}
	   
	   
/******************************************************************************************************************************************/
		List<BuyOrder> orders = system.getRegister();//LISTA DE ORDENES REGISTRADAS EN EL SISTEMA
/******************************************************************************************************************************************/
		System.out.println("*******IMPORTE DE CADA ORDEN DE COMPRA*********************");
		
		
		List<Item> items = null;
		
		
		for (BuyOrder buyOrder : orders) {
			
			items = buyOrder.getProducts();
			float total = 0;
			
			for (Item p : items) {
				
				total += p.getPrice() * p.getQuantity();
				
			}
			
			System.out.println("\nImporte de la Orden de compra N� " + buyOrder.getNumber() + "= " + total + "$");
			
		}	
		
		
		System.out.println("\n");
		System.out.println("*******LISTA DE ORDENES DE COMPRA ORDENADAS ***********");
		
		
		Collections.sort(orders);
		
		
		System.out.println(orders);
		
		
		System.out.println("**********ORDENAMIENTO POR APELLIDO PROVEEDOR*********************************");
		
		
		Collections.sort(orders, new Comparator<BuyOrder>() {

			@Override
			public int compare(BuyOrder o1, BuyOrder o2) {
				
				return o1.getProvider().getLastname().compareTo(o2.getProvider().getLastname());
			}
			
		});
		
		
		System.out.println(orders);
	
			
	} 

}
