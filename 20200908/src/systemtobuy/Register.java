package systemtobuy;

import java.util.ArrayList;
import java.util.List;

import clases.BuyOrder;

//SISTEMA DE ORDEN DE COMPRA
public class Register {
	
	
	private static Register sys;
	private List<BuyOrder> list_order;


	private Register() {}
	
	
	public static Register getInstance() {
		
		if(sys == null) {
		
			return sys = new Register();
		
		}
		
		return sys;
	
	}
	

	public List<BuyOrder> getRegister() {return list_order;}

	
	public void register(BuyOrder order) {
		
		if(list_order == null) {
			
			list_order = new ArrayList<>();
		
		}
		
		list_order.add(order);	
		
	}
	
	

}
