package dataEnum;
//PRODUCTOS CONSTANTES
public enum Items {
	
	
	TABLET_LG("Sony Z10", 19999.99f),
	CELULAR("Samsung galaxy s20", 18000),
	CARGADOR("Cargador Noblex", 200),
	TABLET_SONY("Celular Sony", 19999.99f),
	CELULAR_LG("Celular LG", 21000),
	CELULAR_MOTOROLA("Motorola v8", 5000),
	TABLET_NEXT("Tablet next", 8599.99f),
	TABLET_NOGA("Tablet noga", 7000),
	TECLADO_DRAGON("Teclado Redragon", 3000),
	FUNDA_SONY("Funda Sony", 300),
	AURICULARES_INALAMNRICO("Auriculares sin cable Philips", 800),
	CABLE_RED("Cable de red 3m", 250),
	PARLANTE("Parlantes Nisuta", 1200),
	AURICULAR_MICROFONO("Auriculares con microfono", 20000),
	CAMARA("Camara", 5000);
	
	
	private String description;
	private float price;
	
	
	Items(String description, float price) {
		
		this.description = description;
		this.price = price;
	
	}

	
	public String getDescription() {return description;}

	
	public void setDescription(String description) {this.description = description;}

	
	public float getPrice() {return price;}

	
	public void setPrice(float price) {this.price = price;}
	
	
}
