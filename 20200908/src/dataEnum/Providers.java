package dataEnum;
//PROVEEDORES CONSTANTES
public enum Providers {
	
	
	JUAN("CORDOBA", "AV XXXXXX"),
	DANIELA("PEREZ", "AV XXXXXX"),
	BRENDA("MENDOZA", "AV XXXXXX"),
	ALAN("GOMEZ", "AV XXXXXX"),
	MARTIR("RUIZ", "AV XXXXXX"),
	LAURA("RUIZ", "AV XXXXXX"),
    GONZALO("PEREZ", "AV XXXXXX"),
	LAUTARO("GONZALES", "AV XXXXXX"),
	PEDRO("CORDOBA", "AV XXXXXX"),
	IGNACIO("RODRIGUEZ", "AV XXXXXX"),
	SERGIO("CORDOBA", "AV XXXXXX"),
	LEANDRO("JUAREZ", "AV XXXXXX"),
	RICARDO("GOMEZ", "AV XXXXXX");
	
	
	private String lastName;
	private String address;
	
	
	Providers(String lastName, String address){
		
		this.lastName = lastName;
		this.address = address;
	
	}

	
	public String getLastName() {return lastName;}
	

	public void setLastName(String lastName) {this.lastName = lastName;}
	

	public String getAddress() {return address;}

	
	public void setAddress(String address) {this.address = address;}
	

}
