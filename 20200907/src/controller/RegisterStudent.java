package controller;

import java.util.HashSet;
import java.util.Set;

public class RegisterStudent implements RegisterUser{

	
	private Set<Student> students;
	private Student userStudent; 
	
	public RegisterStudent() {}


	@Override
	public void register(Object user) throws Exception {
		
		userStudent = (Student)user;	
		
		if(students != null) {
			
			students = new HashSet<>();
			
		}
		//VERIFICO SI EL ESTUDIANTE SE INGRESO CON EXCITO
		if(students.contains(userStudent)) {
			
			throw new Exception("NO SE PUDO INGRESAR AL NUEVO ESTUDIATE");
			
		}
		
		students.add(userStudent);	
	}


	public Set<Student> getStudents() {return students;}


	
	
	
	
	
	

	

}
