package controller;

public class Exam {
	
	private float note;
	private String description;
	

	public float getNote() {return note;}
	

	public void setNote(float note) {this.note = note;}


	public String getDescription() {return description;}


	public void setDescription(String description) {this.description = description;}

	
	
}
