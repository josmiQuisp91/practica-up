package controller;

import java.util.HashSet;
import java.util.Set;

public class RegisteTeacher implements RegisterUser {
	
	
	private Set<Teacher> userTeacher;
	private Teacher teacher;

	@Override
	public void register(Object user) throws Exception {
		
		teacher = (Teacher)user;
		
		if(userTeacher == null) {
			
			userTeacher = new HashSet<>();
			
		}

		
		if(userTeacher.contains(teacher)) {
			
			throw new Exception("NO SE PUDO INGRESAR AL DOCENTE");
		}
		
		userTeacher.add(teacher);
		
	}
	

	public Set<Teacher> getUserTeacher() {return userTeacher;}
	
	

}
